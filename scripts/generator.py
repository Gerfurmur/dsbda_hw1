import random
import argparse
from time import time


MAX_VALUE = 10
MIN_VALUE = -10

BYTE = 1
KBYTE = 1024 * BYTE
MBYTE = 1024 * KBYTE

MAX_COLUMNS = 15
MIN_COLUMNS = 4

SEPARATOR = " "
SEPARATOR_SIZE = len(SEPARATOR)


def generate_data(file_name, file_size):
    sttime = time()
    file_size_in_bytes = file_size * MBYTE
    current_size = 0
    columns_num = random.randint(MIN_COLUMNS, MAX_COLUMNS)

    with open(file_name, 'w') as wf:
        buf = []
        lst = []

        while current_size < file_size_in_bytes:
            value = str(random.randint(MIN_VALUE, MAX_VALUE))
            value_size = len(value) + SEPARATOR_SIZE  # каждый символ - 1 байт, плюс разделитель между числами - SEPARATOR_SIZE байт
            current_size += value_size

            lst.append(value)

            if len(lst) < columns_num:
                continue

            buf.append(SEPARATOR.join(lst))
            lst = []

        wf.write('\n'.join(buf))

    print("OUTPUT FILE NAME:\t%s" % file_name)
    print("OUTPUT FILE SIZE:\t%s Mb" % file_size)
    print("COLUMN SIZE:\t\t%s" % columns_num)
    print("GENERATION TIME:\t%s s" % (time() - sttime))


def arg_parse():
    parser = argparse.ArgumentParser(description="Data generator script")

    parser.add_argument("-s", "--size", help="Size of generated file in Mbytes", dest="file_size", required=True, type=float)
    parser.add_argument("-o", "--output", help="file name", dest="file_name", required=False, default='data.csv', type=str)

    return parser.parse_args()


def main():
    args = arg_parse()
    generate_data(args.file_name, args.file_size)


if __name__ == "__main__":
    main()


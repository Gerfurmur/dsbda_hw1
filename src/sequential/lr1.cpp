#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <chrono>
#include "dataprocessing.h"


const char SEPARATOR = ' ';


// Сохранение результатов
void save_matrix(std::vector<std::vector<int>> matrix, double work_time, char *file_name) {
	struct stat file_stat;
	stat(file_name, &file_stat);
	size_t file_size = file_stat.st_size;
	
	char output_file_name[40];
	sprintf(output_file_name, "%s.out", file_name);
	FILE *out_file = fopen(output_file_name, "w");
	if (out_file == NULL)
		std::cerr << "error: output file open failed '" << output_file_name << "'.\n";
	else {
		int N = (int)matrix.size();
		int M = 0;
		
		for (int i = 0; i < N; i++) {
			M = (int)matrix[i].size();

			for(int j = 0; j < M; j++)
				fprintf(out_file, "%d ", matrix[i][j]);
			
			fprintf(out_file, "\n");
		}
		
		fprintf(out_file, "\nPROCESSING TIME:\t%lf\n", work_time);

		fprintf(out_file, "\nINPUT FILE SIZE:\t%ld\n", file_size);
		
		fclose (out_file);
	}
}


// Выгрузка данных и преобразование их в матрицу
std::vector<std::vector<int>> get_matrix(char *file_name) {
    std::ifstream f;
	f.open(file_name);

    std::vector<std::vector<int>> array;    /* сохранять данные будет в вектор  */
    std::string line, val;

    while (std::getline (f, line)) {        /* считываем строку */
        std::vector<int> v;                 /* формируем вектор интов - строка матрицы */
        std::stringstream s (line);
        while (getline (s, val, SEPARATOR))       /* вынимаем значения с учетом разделителя */
            v.push_back(std::stoi (val));
        array.push_back(v);
    }

	return array;
}



int main(int argc, char * argv[]) {
	if (argc < 3) {
		printf("Usage: %s <filename> <k>\n", argv[0]);
		return -1;
	}

	char * file_name = argv[1];
	if (-1 == open(file_name, O_RDONLY)) {    // Проверяем, что файл открывается
		std::cerr << "error: file open failed '" << file_name << "'.\n";
		return 1;
	}

	int k = atoi(argv[2]);
	if (k < 1 || k > 10) {		 // Проверяем входной параметр
		std::cerr << "error: input parameter k =  " << k << "is invalid.\n";
		return 1;
	}
	
	
	std::vector<std::vector<int>> matrix = get_matrix(file_name);
	
	std::cout << "INPUT FILE NAME:\t'" << file_name << "'\n";
	std::cout << "INPUT PARAMETER k:\t" << k << "\n";
	// set work_time_start
	auto begin = std::chrono::high_resolution_clock::now();
	
	// start processing
	matrix_processing(matrix, k);
	
	// get work time
	auto end = std::chrono::high_resolution_clock::now();
  
	auto elapsed_ms = std::chrono::duration_cast<std::chrono::milliseconds>(end - begin);
	std::cout << "Work time:\t" << elapsed_ms.count() << " ms\n";
	
	// save results
	save_matrix(matrix, (double)elapsed_ms.count(), file_name);
	
	return 0;
}


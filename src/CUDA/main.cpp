#include <cstdio>
#include <cstdlib>
#include <string.h>
#include <sys/stat.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <chrono>
#include "dataprocessing.cuh"


const char SEPARATOR = ' ';
int MATRIX_COLUMN_SIZE = 0;


// ���������� �����������
void save_matrix(std::vector<long long int> matrix, int col_size, double work_time, char* file_name) {
	struct stat file_stat;
	stat(file_name, &file_stat);
	size_t file_size = file_stat.st_size;

	char output_file_name[40];
	sprintf(output_file_name, "%s.out", file_name);
	FILE* out_file = fopen(output_file_name, "w");
	if (out_file == NULL)
		std::cerr << "error: output file open failed '" << output_file_name << "'.\n";
	else {
		int N = (int)matrix.size();
		int j = 0;

		for (int i = 0; i < N; i++) {
			fprintf(out_file, "%lld ", matrix[i]);
			++j;
			if (j == col_size) {
				fprintf(out_file, "\n");
				j = 0;
			}
		}

		fprintf(out_file, "\nPROCESSING TIME:\t%lf\n", work_time);

		fprintf(out_file, "\nINPUT FILE SIZE:\t%lld\n", file_size);

		fclose(out_file);
	}
}


// �������� ������ � �������������� �� � �������
std::vector<long long int> get_matrix(char* file_name) {
	std::ifstream f;
	f.open(file_name);

	std::vector<long long int> array;    /* ��������� ������ ����� � ������  */
	std::string line, val;

	std::getline(f, line);
	MATRIX_COLUMN_SIZE = std::stoi(line);

	while (std::getline(f, line)) {        // ��������� ������ //
		std::stringstream s(line);
		while (getline(s, val, SEPARATOR)) // �������� �������� � ������ ����������� //
			array.push_back(std::stoll(val));
	}

	return array;
}



int main(int argc, char* argv[]) {
	if (argc < 3) {
		std::cerr << "Usage: " << argv[0] << " <filename> <k>\n";
		return -1;
	}

	char* file_name = argv[1];
	FILE* out_file = fopen(file_name, "r");
	if (out_file == NULL) {    // ���������, ��� ���� �����������
		std::cerr << "error: file open failed '" << file_name << "'.\n";
		return 1;
	}
	else fclose(out_file);

	int k = atoi(argv[2]);
	if (k < 1 || k > 10) {		 // ��������� ������� ��������
		std::cerr << "error: input parameter k =  " << k << "is invalid.\n";
		return 1;
	}

	std::vector<long long int> matrix = get_matrix(file_name);

	std::cout << "INPUT FILE NAME:\t'" << file_name << "'\n";
	std::cout << "INPUT PARAMETER k:\t" << k << "\n";
	int num_rows = (int)(matrix.size() / MATRIX_COLUMN_SIZE);
	int num_cols = MATRIX_COLUMN_SIZE;

	// set work_time_start
	auto begin_steady = std::chrono::steady_clock::now();

	// start processing
	float cuda_time = matrix_processing(&matrix[0], k, num_rows, num_cols);

	// get work time
	auto end_steady = std::chrono::steady_clock::now();

	std::chrono::duration<double> elap = end_steady - begin_steady;
	std::cout << "Work time:\t" << elap.count() << " ms\n";
	std::cout << "CUDA time:\t" << cuda_time << " ms\n";

	// save results
	save_matrix(matrix, MATRIX_COLUMN_SIZE, (double)elap.count(), file_name);

	return 0;
}


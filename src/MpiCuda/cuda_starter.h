#pragma once
#include <vector>
float cuda_start(std::vector<long long int>& matrix, long long int k_param, int num_rows, int num_cols);
//void mpi_matrix_processing(std::vector<long long int>& matrix, long long int k_param, int num_rows, int num_cols);
void mpi_matrix_processing(std::vector<long long int>& matrix, long long int k, int col_size, long long int from, long long int to);


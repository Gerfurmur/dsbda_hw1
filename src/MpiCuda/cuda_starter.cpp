#include <vector>
#include "dataprocessing.cuh"
#include "cuda_starter.h"


float cuda_start(std::vector<long long int> & matrix, long long int k_param, int num_rows, int num_cols) {
	// start processing
	float cuda_time = cuda_matrix_processing(&matrix[0], k_param, num_rows, num_cols);
	return cuda_time;
}



// ���������
long long int fact(long long int n) {
	if (0 == n)
		return 1;
	
	return n * fact(n - 1);
}


// ��������� ������ � ������������ � ���������
//void mpi_matrix_processing(std::vector<long long int>& matrix, long long int k_param, int num_rows, int num_cols) {
//	long long int i, j, p;
//	int num_elements = num_rows * num_cols;
//
//	for (i = 0; i < num_elements; i += num_cols) {
//		p = 0;
//		for (j = i; j < i + num_cols; ++j) {
//			if (matrix[j] > 0)
//				++p;
//		}
//
//		p = fact(p + k_param);
//
//		for (j = i; j < i + num_cols; ++j)
//			matrix[j] = p * matrix[j];
//	}
//}


// ��������� ������ � ������������ � ���������
void mpi_matrix_processing(std::vector<long long int>& matrix, long long int k, int col_size, long long int from, long long int to) {
	long long int i, j, p;

	for (i = from; i < to; i += col_size) {
		p = 0;
		for (j = i; j < i + col_size; ++j) {
			if (matrix[j] > 0)
				++p;
		}

		p = fact(p + k);

		for (j = i; j < i + col_size; ++j)
			matrix[j] = p * matrix[j];
	}
}


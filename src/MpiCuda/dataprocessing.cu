﻿#include "dataprocessing.cuh"
#include <vector>
#include <cuda.h>
#include <iostream>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"


#define checkCudaErrors(val) check( (val), #val, __FILE__, __LINE__)
#define WARP_SIZE 32


template<typename T>
void check(T err, const char* const func, const char* const file, const int line) {
	if (err != cudaSuccess) {
		std::cerr << "CUDA error at: " << file << ":" << line << std::endl;
		std::cerr << cudaGetErrorString(err) << " " << func << std::endl;
		exit(1);
	}
}


__global__
void row_processing(long long int* d_matrix, long long int k_param, int num_rows, int num_cols, int elems_per_thread)
{
	// грубо говоря - диапазон номеров строк, откуда начинаем - где заканчиваем
	int from = (blockDim.x * blockIdx.x + threadIdx.x) * elems_per_thread;
	int to = from + elems_per_thread;

	// поправка на то, что матрица в одномерном массиве
	from *= num_cols;
	to *= num_cols;

	long long int i, j, k, p, fact;
	int num_elements = num_rows * num_cols;

	for (i = from; i < to && i < num_elements; i += num_cols) {
		p = 0;
		fact = 1;
		for (j = i; j < i + num_cols; ++j) {
			if (d_matrix[j] > 0)
				++p;
		}

		for (k = 1; k <= (p + k_param); ++k)
			fact *= k;

		for (j = i; j < i + num_cols; ++j)
			d_matrix[j] = fact * d_matrix[j];
	}
}


// обработка данных в соответствии с вариантом
float cuda_matrix_processing(long long int* h_matrix, long long int k_param, int num_rows, int num_cols) {
	long long int* d_matrix;

	const int num_elements = num_rows * num_cols;
	cudaSetDevice(0);
	checkCudaErrors(cudaGetLastError());

	//allocate memory on the device
	checkCudaErrors(cudaMalloc(&d_matrix, sizeof(long long int) * num_elements));

	//copy input array to the GPU
	checkCudaErrors(cudaMemcpy(d_matrix, h_matrix, sizeof(long long int) * num_elements, cudaMemcpyHostToDevice));

	dim3 blockSize;
	dim3 gridSize;
	int threadNum;

	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);

	threadNum = 256;
	const int elems_per_thread = 32;

	blockSize = dim3(threadNum, 1, 1);
	gridSize = dim3(num_rows / (threadNum * elems_per_thread) + 1, 1, 1);

	cudaEventRecord(start);

	// calculate
	row_processing <<<gridSize, blockSize >>> (d_matrix, k_param, num_rows, num_cols, elems_per_thread);
	//rgba_to_grayscale_optimized<<<gridSize, blockSize>>>(d_imageRGBA, d_imageGray, numRows, numCols, elems_per_thread);

	cudaEventRecord(stop);
	cudaEventSynchronize(stop);
	cudaDeviceSynchronize();

	checkCudaErrors(cudaGetLastError());

	float milliseconds = 0;
	cudaEventElapsedTime(&milliseconds, start, stop);
	//std::cout << "CUDA time: " << milliseconds << " (ms)\n";

	checkCudaErrors(cudaMemcpy(h_matrix, d_matrix, sizeof(long long int) * num_elements, cudaMemcpyDeviceToHost));

	cudaFree(d_matrix);

	return milliseconds;
}


#include <cstdio>
#include <cstdlib>
#include <string.h>
#include <sys/stat.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <chrono>
#include "cuda_starter.h"
#include <mpi.h>


#define CUDA_PART_SIZE 9
#define MPI_PART_SIZE 1

const int PARTS = CUDA_PART_SIZE + MPI_PART_SIZE;
const char SEPARATOR = ' ';
int MATRIX_COLUMN_SIZE = 0;


long long int get_range(long long int num_elements, int col_size, int num_procs, int rank, long long int& from, long long int& to) {
	long long int num_rows = num_elements / col_size;
	long long int part_size = num_rows / num_procs;

	from = part_size * rank * col_size;

	if (rank == num_procs - 1) {
		to = num_elements;
	}
	else {
		to = (part_size * (rank + 1)) * col_size;
	}

	return to - from;
}


// ���������� �����������
void save_matrix(std::vector<long long int> matrix, int col_size, double work_time, char* file_name) {
	struct stat file_stat;
	stat(file_name, &file_stat);
	size_t file_size = file_stat.st_size;

	char output_file_name[40];
	sprintf(output_file_name, "%s.out", file_name);
	FILE* out_file = fopen(output_file_name, "w");
	if (out_file == NULL)
		std::cerr << "error: output file open failed '" << output_file_name << "'.\n";
	else {
		int N = (int)matrix.size();
		int j = 0;

		for (int i = 0; i < N; i++) {
			fprintf(out_file, "%lld ", matrix[i]);
			++j;
			if (j == col_size) {
				fprintf(out_file, "\n");
				j = 0;
			}
		}

		fprintf(out_file, "\nPROCESSING TIME:\t%lf\n", work_time);

		fprintf(out_file, "\nINPUT FILE SIZE:\t%lld\n", file_size);

		fclose(out_file);
	}
}


// �������� ������ � �������������� �� � �������
std::vector<long long int> get_matrix(char* file_name) {
	std::ifstream f;
	f.open(file_name);

	std::vector<long long int> array;    /* ��������� ������ ����� � ������  */
	std::string line, val;

	std::getline(f, line);
	MATRIX_COLUMN_SIZE = std::stoi(line);

	while (std::getline(f, line)) {        // ��������� ������ //
		std::stringstream s(line);
		while (getline(s, val, SEPARATOR)) // �������� �������� � ������ ����������� //
			array.push_back(std::stoll(val));
	}

	return array;
}



int main(int argc, char* argv[]) {
	if (argc < 3) {
		std::cerr << "Usage: " << argv[0] << " <filename> <k>\n";
		return -1;
	}

	char* file_name = argv[1];
	FILE* out_file = fopen(file_name, "r");
	if (out_file == NULL) {    // ���������, ��� ���� �����������
		std::cerr << "error: file open failed '" << file_name << "'.\n";
		return 1;
	}
	else fclose(out_file);

	int k = atoi(argv[2]);
	if (k < 1 || k > 10) {		 // ��������� ������� ��������
		std::cerr << "error: input parameter k =  " << k << "is invalid.\n";
		return 1;
	}

	/* MPI START */
	int mpi_rank, mpi_size;

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
	MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
	MPI_Status status;

	std::chrono::steady_clock::time_point begin_steady;
	float cuda_time;
	int num_rows = 1, num_cols = 1;
	long long int MPI_Num_elements = 0, from, to, num_elements;
	std::vector<long long int> row;
	std::vector<long long int> matrix;

	int root = 0;
	// divide task onto big part for CUDA and small part for MPI subprocess
	int cuda_num_rows;
	int mpi_num_rows;

	if (mpi_rank == root) {
		matrix = get_matrix(file_name);

		std::cout << "INPUT FILE NAME:\t'" << file_name << "'\n";
		std::cout << "INPUT PARAMETER k:\t" << k << "\n";
		num_elements = (long long int)matrix.size();
		num_rows = (int)(num_elements / MATRIX_COLUMN_SIZE);
		num_cols = MATRIX_COLUMN_SIZE;

		// divide task onto big part for CUDA and small part for MPI subprocess
		cuda_num_rows = num_rows * CUDA_PART_SIZE / PARTS;
		mpi_num_rows = num_rows - cuda_num_rows;
		MPI_Num_elements = mpi_num_rows * num_cols;

		row.resize(MPI_Num_elements);
		memcpy(&row[0], &matrix[num_elements - MPI_Num_elements], MPI_Num_elements * sizeof(long long int));
	}

	MPI_Barrier(MPI_COMM_WORLD);

	// Send MPI_Num_elements, num_cols
	MPI_Bcast(&MPI_Num_elements, 1, MPI_LONG_LONG_INT, root, MPI_COMM_WORLD);
	MPI_Bcast(&num_cols, 1, MPI_INT, root, MPI_COMM_WORLD);

	//std::cout << "RANK " << mpi_rank << "\tNUM ELEMENTS " << MPI_Num_elements << "\n";
	//std::cout << "RANK " << mpi_rank << "\tnum_cols " << num_cols << "\n";
	
	long long int range_length = get_range(MPI_Num_elements, num_cols, mpi_size, mpi_rank, from, to);

	if (mpi_rank != root)
		row.resize(MPI_Num_elements);

	// Send MPI PART Matrix
	MPI_Bcast(&row[0], MPI_Num_elements, MPI_LONG_LONG_INT, root, MPI_COMM_WORLD);
	
	if (mpi_rank == root) {
		long long int my_part_cols_num = (range_length / num_cols);
		cuda_num_rows += my_part_cols_num;
		// set work_time_start
		begin_steady = std::chrono::steady_clock::now();

		// start processing
		cuda_time = cuda_start(matrix, k, cuda_num_rows, num_cols);
		cuda_num_rows -= my_part_cols_num;
	} else {
		mpi_matrix_processing(row, k, num_cols, from, to);
	}

	MPI_Barrier(MPI_COMM_WORLD);

	/*MPI_Barrier(MPI_COMM_WORLD);
	MPI_Finalize();
	return 0;*/
	

	if (mpi_rank == root) {
		// get work time
		auto end_steady = std::chrono::steady_clock::now();

		long long int cuda_offset = cuda_num_rows * num_cols;

		// get results from MPI procs
		for (int i = 0; i < mpi_size; ++i) {
			if (i == root)
				continue;
			range_length = get_range(MPI_Num_elements, num_cols, mpi_size, i, from, to);
			if ((cuda_offset + from + range_length) > num_elements)
				continue;
			MPI_Recv(&matrix[cuda_offset + from], range_length, MPI_LONG_LONG_INT, i, i, MPI_COMM_WORLD, &status);
		}

		std::chrono::duration<double> elap = end_steady - begin_steady;
		std::cout << "Work time:\t" << elap.count() << " s\n";
		std::cout << "CUDA time:\t" << cuda_time << " ms\n";

		// save results
		save_matrix(matrix, MATRIX_COLUMN_SIZE, (double)elap.count(), file_name);
	} else {
		MPI_Send(&row[from], range_length, MPI_LONG_LONG_INT, root, mpi_rank, MPI_COMM_WORLD);
	}
	
	MPI_Finalize();

	return 0;
}


#include "dataprocessing.h"
#include <vector>
#include <omp.h>

#define NUM_THREADS 4

// Факториал
int fact(int n) {
	if (0 == n)
		return 1;
	else
		return n * fact(n-1);
}


std::vector<int> row_processing(std::vector<int> & row, int k) {
	int p = 0;
	int M = (int)row.size();

	for(const int& i : row)
		if (i > 0)
			++p;

	p = fact(k + p);

	for(int j = 0; j < M; j++)
		row[j] = p * row[j];

	return row;
}


// обработка данных в соответствии с вариантом
void matrix_processing(std::vector<std::vector<int>> & matrix, int k, int n_threads) {
	int N = (int)matrix.size();
	
	omp_set_num_threads(n_threads);
	# pragma omp parallel shared(matrix, k, N) //num_threads(NUM_THREADS)
	{
		//int tid = omp_get_thread_num();
		
		# pragma omp for
		for(int i = 0; i < N; i++)
			row_processing(matrix[i], k);
	}
}



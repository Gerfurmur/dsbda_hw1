#include <cstdio>
#include <cstdlib>
#include <string.h>
#include <sys/stat.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <chrono>
#include <mpi.h>


int MATRIX_COLUMN_SIZE = 0;
const char SEPARATOR = ' ';


// Факториал
long long int fact(long long int n) {
	if (0 == n)
		return 1;
	else
		return n * fact(n-1);
}


// обработка данных в соответствии с вариантом
/*
void matrix_processing(std::vector<long long int> & matrix, int k, int col_size) {
	long long int p = 0;
	long unsigned int matrix_size = matrix.size(), i, j;
	
	for(i = 0; i < matrix_size; i += col_size) {
		p = 0;
		for(j = i; j < i + col_size; ++j) {
			if (matrix[j] > 0)
				++p;
		}
		
		p = fact(p + k);
		
		for(j = i; j < i + col_size; ++j)
			matrix[j] = p * matrix[j];
	}
}
*/


// обработка данных в соответствии с вариантом
void matrix_processing(std::vector<long long int> & matrix, int k, int col_size, long long int from, long long int to) {
	long long int i, j, p = 0;
	
	for(i = from; i < to; i += col_size) {
		p = 0;
		for(j = i; j < i + col_size; ++j) {
			if (matrix[j] > 0)
				++p;
		}
		
		p = fact(p + k);
		
		for(j = i; j < i + col_size; ++j)
			matrix[j] = p * matrix[j];
	}
}


long long int get_range(long long int num_elements, int col_size, int num_procs, int rank, long long int & from, long long int & to) {
	long long int num_rows = num_elements / col_size;
	long long int part_size = num_rows / num_procs;

	from = part_size * rank * col_size;
	
	if (rank == num_procs - 1) {
		to = num_elements;
	} else {
		to = (part_size * (rank + 1)) * col_size;
	}
	
	return to - from;
}


// Сохранение результатов
void save_matrix(std::vector<long long int> matrix, int col_size, double work_time, char *file_name) {
	struct stat file_stat;
	stat(file_name, &file_stat);
	size_t file_size = file_stat.st_size;
	
	char output_file_name[40];
	sprintf(output_file_name, "%s.out", file_name);
	FILE *out_file = fopen(output_file_name, "w");
	if (out_file == NULL)
		std::cerr << "error: output file open failed '" << output_file_name << "'.\n";
	else {
		int N = (int)matrix.size();
		int j = 0;
		
		for (int i = 0; i < N; i++) {
			fprintf(out_file, "%lld ", matrix[i]);
			++j;
			if (j == col_size) {
				fprintf(out_file, "\n");
				j = 0;
			}
		}
		
		fprintf(out_file, "\nPROCESSING TIME:\t%lf\n", work_time);

		fprintf(out_file, "\nINPUT FILE SIZE:\t%ld\n", file_size);
		
		fclose (out_file);
	}
}


// Выгрузка данных и преобразование их в матрицу
std::vector<long long int> get_matrix(char *file_name) {
	std::ifstream f;
	f.open(file_name);

	std::vector<long long int> array;    /* сохранять данные будет в вектор  */
	std::string line, val;
	
	std::getline (f, line);
	MATRIX_COLUMN_SIZE = std::stoi(line);
	
	/*
	std::getline (f, line);
	std::stringstream s (line);
	while (getline(s, val, SEPARATOR))  // вынимаем значения с учетом разделителя //
		    array.push_back(std::stoi (val));
	*/
	
	while (std::getline (f, line)) {        // считываем строку //
		//std::vector<int> v;     // формируем вектор интов - строка матрицы //
		std::stringstream s(line);
		while (getline(s, val, SEPARATOR)) // вынимаем значения с учетом разделителя //
		    array.push_back(std::stoll (val));
		//array.push_back(v);
	}	

	return array;
}



int main(int argc, char * argv[]) {
	if (argc < 3) {
		std::cerr << "Usage: " << argv[0] << " <filename> <k>\n";
		return -1;
	}

	char * file_name = argv[1];
	FILE *out_file = fopen(file_name, "r");
	if (out_file == NULL) {    // Проверяем, что файл открывается
		std::cerr << "error: file open failed '" << file_name << "'.\n";
		return 1;
	} else fclose (out_file);

	int k_param = atoi(argv[2]);
	if (k_param < 1 || k_param > 10) {		 // Проверяем входной параметр
		std::cerr << "error: input parameter k =  " << k_param << "is invalid.\n";
		return 1;
	}
	
	std::chrono::time_point<std::chrono::system_clock> begin, end;
	int rank, size;
	double elapsed_ms;
	
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Status status;
	std::vector<long long int> row;
	std::vector<long long int> matrix;
	long long int Num_elements, from, to;
	int Col_size;
	int root = 0;
	
	
	if (rank == root) {
		matrix = get_matrix(file_name);
		Col_size = MATRIX_COLUMN_SIZE;
		Num_elements = matrix.size();
		
		std::cout << "INPUT FILE NAME:\t'" << file_name << "'\n";
		std::cout << "INPUT PARAMETER k:\t" << k_param << "\n";
	
		std::cout << "RANK " << rank << "\tNUM ELEMENTS:\t" << Num_elements << "\n";
	}
	
	
	// send Num_elements to other processes
	MPI_Bcast(&Num_elements, 1, MPI_LONG_LONG_INT, root, MPI_COMM_WORLD);
	// send Col_size to other processes
	MPI_Bcast(&Col_size, 1, MPI_INT, root, MPI_COMM_WORLD); 
	
	long long int range_length = get_range(Num_elements, Col_size, size, rank, from, to);
	
	if (rank != root)
		matrix.resize(Num_elements);
	
	
	// send data for processing
	//MPI_Scatter(&matrix[0], range_length, MPI_LONG_LONG_INT, &row[0], range_length, MPI_LONG_LONG_INT, root, MPI_COMM_WORLD);
	MPI_Bcast(&matrix[0], Num_elements, MPI_LONG_LONG_INT, root, MPI_COMM_WORLD);
	std::cout << "RANK " << rank << "\tCol_size:\t" << Col_size << "\n";
	std::cout << "RANK " << rank << "\tfrom:\t" << from << "\n";
	std::cout << "RANK " << rank << "\tto:\t" << to << "\n";
	
	
	if (rank == root) {
		// set work_time_start
		begin = std::chrono::high_resolution_clock::now();
	}
	
	
	// matrix_processing
	matrix_processing(matrix, k_param, Col_size, from, to);
	//matrix_processing(row, k_param, Col_size);
	
	//row.resize(range_length);
	
	if (rank != root)
		for(auto i = from; i < to; ++i)
			row.push_back(matrix[i]);
	
	// wait for all
	MPI_Barrier(MPI_COMM_WORLD);	
	
	// send_results
	if (rank == root) {
		// get work time
		end = std::chrono::high_resolution_clock::now();
	  
		elapsed_ms = (double)(std::chrono::duration_cast<std::chrono::milliseconds>(end - begin)).count();
		std::cout << "Work time:\t" << elapsed_ms << " ms\n";

		for (int i = 0; i < size; ++i) {
			if (i == root)
				continue;
			range_length = get_range(Num_elements, Col_size, size, i, from, to);
			MPI_Recv(&matrix[from], range_length, MPI_LONG_LONG_INT, i, i, MPI_COMM_WORLD, &status);
		}
	} else {
		MPI_Send(&row[0], row.size(), MPI_LONG_LONG_INT, root, rank, MPI_COMM_WORLD);
	}
	//MPI_Gather(&row[0], range_length, MPI_LONG_LONG_INT, &matrix[from], range_length, MPI_LONG_LONG_INT, root, MPI_COMM_WORLD);
	

	MPI_Finalize();
	
	// save matrix
	if (rank == root) {		
		// save results
		save_matrix(matrix, Col_size, elapsed_ms, file_name);
	}
	
	return 0;
}


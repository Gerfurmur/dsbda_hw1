#include <cstdio>
#include <cstdlib>
#include <string.h>
#include <sys/stat.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <chrono>
#include <mpi.h>
#include "dataprocessing.h"


const char SEPARATOR = ' ';


// Сохранение результатов
void save_matrix(std::vector<std::vector<int>> matrix, double work_time, char *file_name) {
	struct stat file_stat;
	stat(file_name, &file_stat);
	size_t file_size = file_stat.st_size;
	
	char output_file_name[40];
	sprintf(output_file_name, "%s.out", file_name);
	FILE *out_file = fopen(output_file_name, "w");
	if (out_file == NULL)
		std::cerr << "error: output file open failed '" << output_file_name << "'.\n";
	else {
		int N = (int)matrix.size();
		int M = 0;
		
		for (int i = 0; i < N; i++) {
			M = (int)matrix[i].size();

			for(int j = 0; j < M; j++)
				fprintf(out_file, "%d ", matrix[i][j]);
			
			fprintf(out_file, "\n");
		}
		
		fprintf(out_file, "\nPROCESSING TIME:\t%lf\n", work_time);

		fprintf(out_file, "\nINPUT FILE SIZE:\t%ld\n", file_size);
		
		fclose (out_file);
	}
}


// Выгрузка данных и преобразование их в матрицу
std::vector<std::vector<int>> get_matrix(char *file_name) {
    std::ifstream f;
	f.open(file_name);

    std::vector<std::vector<int>> array;    /* сохранять данные будет в вектор  */
    std::string line, val;

    while (std::getline (f, line)) {        /* считываем строку */
        std::vector<int> v;                 /* формируем вектор интов - строка матрицы */
        std::stringstream s (line);
        while (getline (s, val, SEPARATOR))       /* вынимаем значения с учетом разделителя */
            v.push_back(std::stoi (val));
        array.push_back(v);
    }

	return array;
}



int main(int argc, char * argv[]) {
	if (argc < 3) {
		std::cerr << "Usage: " << argv[0] << " <filename> <k>\n";
		return -1;
	}

	char * file_name = argv[1];
	FILE *out_file = fopen(file_name, "r");
	if (out_file == NULL) {    // Проверяем, что файл открывается
		std::cerr << "error: file open failed '" << file_name << "'.\n";
		return 1;
	} else fclose (out_file);

	int k_param = atoi(argv[2]);
	if (k_param < 1 || k_param > 10) {		 // Проверяем входной параметр
		std::cerr << "error: input parameter k =  " << k_param << "is invalid.\n";
		return 1;
	}
	
	std::chrono::time_point<std::chrono::system_clock> begin, end;
	int rank, size;
	clock_t start;
	
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Status status;
	std::vector<int> row;
	std::vector<std::vector<int>> matrix;
	int N = 0, M = 0;
	
	if (rank == 0) {
		matrix = get_matrix(file_name);
		
		std::cout << "INPUT FILE NAME:\t'" << file_name << "'\n";
		std::cout << "INPUT PARAMETER k:\t" << k_param << "\n";
		N = (int)matrix.size();
		M = (int)matrix[0].size();
		std::cout << "row_nums k:\t" << N << "\n";
		std::cout << "col_size k:\t" << M << "\n";
	}
	
	// send N - row_nums to other processes
	MPI_Bcast(&N, 1, MPI_INT, 0, MPI_COMM_WORLD);
	
	// send M - col_size to other processes
	MPI_Bcast(&M, 1, MPI_INT, 0, MPI_COMM_WORLD);
	row.resize(M);
	
	if (rank == 0) {
		// set work_time_start
		begin = std::chrono::high_resolution_clock::now();
		start = clock();

		if (size > 1) {
			int i = 0, j = 0, j1 = 0, k = 0;
			
			while (i < N) {
				//row = matrix[i];
				++i;
				j = 1;
				while (j < size && i < N) {
					MPI_Send(&matrix[i][0], M, MPI_INT, j, j, MPI_COMM_WORLD);
					++i;
					++j;
				}
				row_processing(matrix[i - size], k_param);
				j1 = 1;
				while (j1 < j) {
					MPI_Recv(&row[0], M, MPI_INT, j1, j1, MPI_COMM_WORLD, &status);
					for(k = 0; k < M; ++k)
						matrix[i - (size - j1)][k] = row[k];
					++j1;
				}
			}
			
			for(j = 1; j < size; ++j)
				MPI_Send(&j, 1, MPI_INT, j, 0, MPI_COMM_WORLD);
			
		} else {
			// start processing
			matrix_processing(matrix, k_param);
		}
	} else {		
		while (true) {
			MPI_Probe(0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
			
			if (status.MPI_TAG == 0)
				break;
			
			MPI_Recv(&row[0], M, MPI_INT, 0, rank, MPI_COMM_WORLD, &status);
			row_processing(row, k_param);
			MPI_Send(&row[0], M, MPI_INT, 0, rank, MPI_COMM_WORLD);
		}
		
	}
	
	MPI_Finalize();
	
	if (rank == 0) {
		// get work time
		end = std::chrono::high_resolution_clock::now();
		double work_time = (double)(clock() - start) / CLOCKS_PER_SEC;
	  
		auto elapsed_ms = std::chrono::duration_cast<std::chrono::milliseconds>(end - begin);
		std::cout << "Work time:\t" << elapsed_ms.count() << " ms\n";
		std::cout << "Work time CLOCKS:\t" << work_time << " s\n";
		
		// save results
		save_matrix(matrix, (double)elapsed_ms.count(), file_name);
	}
	
	return 0;
}

